Signing a custom kernel for Secure Boot
=======================================


1. _Create the config to create the signing key, save as mokconfig.cnf_

	```
	# This definition stops the following lines failing if HOME isn't
	# defined.
	HOME                    = .
	# RANDFILE                = $ENV::HOME/.rnd
	[ req ]
	distinguished_name      = req_distinguished_name
	x509_extensions         = v3
	string_mask             = utf8only
	prompt                  = no

	[ req_distinguished_name ]
	countryName             = IN
	stateOrProvinceName     = Kar
	localityName            = Ban
	0.organizationName      = 429
	commonName              = Secure Boot Signing Key
	emailAddress            = batman@gmail.com

	[ v3 ]
	subjectKeyIdentifier    = hash
	authorityKeyIdentifier  = keyid:always,issuer
	basicConstraints        = critical,CA:FALSE
	extendedKeyUsage        = codeSigning,1.3.6.1.4.1.311.10.3.6
	nsComment               = "OpenSSL Generated Certificate"
	```

2. _Create the public and private key for signing the kernel_
	```
	openssl req -config ./mokconfig.cnf \
	        -new -x509 -newkey rsa:2048 \
	        -nodes -days 36500 -outform DER \
	        -keyout "MOK.priv" \
	        -out "MOK.der"
	```        
3. _Convert the key also to PEM format (mokutil needs DER, sbsign needs PEM)_

	`openssl x509 -in MOK.der -inform DER -outform PEM -out MOK.pem`

4. _Enroll the key to your shim installation_

	`sudo mokutil --import MOK.der`

	You will be asked for a password, you will just use it to confirm your key selection in the next step, so choose any.


5. _Restart your system_

	You will encounter a blue screen of a tool called MOKManager.   
	Select "Enroll MOK" and then "View key". Make sure it is your key you created in step 2.   
	Afterwards continue the process and you must enter the password which you provided in step 4.    
	Continue with booting your system with the older working kernel.

6. _Verify your key is enrolled via, looks something of this sort_

	```
	[key 1]
	SHA1 Fingerprint: bd:b8:11:67:56:ae:a8:0b:19:35:c8:72:83:09:15:6f:be:e1:35:e8
	Certificate:
	    Data:
	        Version: 3 (0x2)
	        Serial Number:
	            7e:a7:82:c5:ed:f6:3f:0b:5d:06:7f:e3:c8:3b:1c:08:bd:b5:6e:f9
	        Signature Algorithm: sha256WithRSAEncryption
	        Issuer: C=IN, ST=Kar, L=Ban, O=429, CN=Secure Boot Signing Key/emailAddress=batman@gmail.com
	        Validity
	            Not Before: Dec 28 16:52:04 2018 GMT
	            Not After : Dec  4 16:52:04 2118 GMT
	        Subject: C=IN, ST=Kar, L=Ban, O=429, CN=Secure Boot Signing Key/emailAddress=batman@gmail.com
	        Subject Public Key Info:
	            Public Key Algorithm: rsaEncryption
	                RSA Public-Key: (2048 bit)
	                Modulus:
	                    00:da:99:c3:1c:90:c8:b6:60:8d:7a:74:5b:3b:e5:
	                    c1:d0:83:7d:4c:cf:2b:56:0d:af:b6:45:2f:5e:2a:
	                    6f:dc:ba:bf:b9:b8:70:08:2e:3d:e0:0b:1c:a3:30:
	                    85:30:3b:cb:d0:60:34:9b:7e:58:ca:b5:03:6e:fe:
	                    83:7d:d9:c8:83:22:bf:c6:34:fe:ca:84:1d:eb:6d:
	                    79:86:ca:c9:49:7b:91:b6:8b:9b:cb:d4:10:e8:2d:
	                    30:61:64:02:1a:47:35:25:d1:74:7c:df:37:e6:95:
	                    a6:1f:e7:05:27:c9:b7:df:82:12:dd:d7:eb:da:93:
	                    42:04:ac:3f:1b:b6:86:53:88:81:d9:c6:4a:99:60:
	                    fd:c2:39:b1:ba:af:5b:ca:65:23:bf:b6:5b:44:b0:
	                    a2:3d:e6:a5:0d:1a:1b:c1:6f:e2:87:5d:fb:8c:dc:
	                    d3:3f:96:23:40:d9:59:19:72:03:cb:4b:38:75:3d:
	                    14:d6:2f:77:d4:69:65:b2:ab:5b:ed:f4:6a:7c:1f:
	                    8c:0a:6b:55:1d:af:ab:13:87:d5:20:48:f0:c2:66:
	                    2f:15:01:db:39:63:d8:f3:35:ab:ab:37:5a:15:12:
	                    a5:fb:8a:ff:b3:b2:96:3d:24:59:38:f2:de:00:58:
	                    9c:34:b5:3a:31:39:03:47:90:f5:c0:42:47:65:6c:
	                    c1:5f
	                Exponent: 65537 (0x10001)
	        X509v3 extensions:
	            X509v3 Subject Key Identifier: 
	                73:C7:6B:B7:B3:21:8D:09:04:E1:4F:74:DB:CC:98:27:7F:1B:8B:6E
	            X509v3 Authority Key Identifier: 
	                keyid:73:C7:6B:B7:B3:21:8D:09:04:E1:4F:74:DB:CC:98:27:7F:1B:8B:6E

	            X509v3 Basic Constraints: critical
	                CA:FALSE
	            X509v3 Extended Key Usage: 
	                Code Signing, 1.3.6.1.4.1.311.10.3.6
	            Netscape Comment: 
	                OpenSSL Generated Certificate
	    Signature Algorithm: sha256WithRSAEncryption
	         41:b2:98:ef:9a:12:67:94:1b:82:b7:3a:8a:0a:c7:5c:18:5f:
	         7a:c0:e2:71:d6:e9:f9:c2:5c:a8:13:e0:58:5e:50:6b:d3:0c:
	         61:ce:49:47:45:b8:b7:db:8e:d0:0d:5c:62:90:f7:58:3f:40:
	         54:72:11:4b:09:5e:8e:04:4c:87:e7:3a:c6:c5:84:41:a3:df:
	         65:63:7f:51:b7:96:db:17:01:a9:e4:4a:91:93:3f:1f:33:76:
	         45:ce:ee:36:3d:f6:1c:b5:2f:91:68:d1:20:14:a4:5b:57:be:
	         e3:65:b4:63:68:8a:df:b2:42:5b:3e:d9:de:92:0a:22:3f:0b:
	         3e:db:41:ca:55:ac:aa:00:f8:87:74:ad:8c:fc:73:99:c9:b1:
	         b3:02:2c:13:02:c9:09:15:d5:7d:12:ea:58:99:30:1b:a4:fa:
	         12:5e:39:b5:43:12:64:d7:56:5e:04:2c:7e:1b:d1:77:ff:07:
	         77:88:05:af:82:13:c7:7f:4d:b3:6a:ca:64:15:f8:31:73:ae:
	         c2:a4:fd:73:31:b1:77:84:22:ce:85:0e:03:e0:ee:79:a4:5c:
	         13:23:9b:9d:90:65:82:9c:91:3f:39:74:86:4e:3a:f1:7f:c8:
	         0a:22:0d:0c:44:e2:00:8b:eb:de:bc:33:a5:6a:7c:46:4a:0d:
	         bf:0a:c3:5a

	[key 2]
	SHA1 Fingerprint: 76:a0:92:06:58:00:bf:37:69:01:c3:72:cd:55:a9:0e:1f:de:d2:e0
	Certificate:
	    Data:
	        Version: 3 (0x2)
	        Serial Number:
	            b9:41:24:a0:18:2c:92:67
	        Signature Algorithm: sha256WithRSAEncryption
	        Issuer: C=GB, ST=Isle of Man, L=Douglas, O=Canonical Ltd., CN=Canonical Ltd. Master Certificate Authority
	        Validity
	            Not Before: Apr 12 11:12:51 2012 GMT
	            Not After : Apr 11 11:12:51 2042 GMT
	        Subject: C=GB, ST=Isle of Man, L=Douglas, O=Canonical Ltd., CN=Canonical Ltd. Master Certificate Authority
	        Subject Public Key Info:
	            Public Key Algorithm: rsaEncryption
	                RSA Public-Key: (2048 bit)
	                ...
	                ....
	                .....
	                ......
	                .......
	```

7. _Sign your installed kernel_

	`sudo sbsign --key MOK.priv --cert MOK.pem /boot/vmlinuz-4.19.0-batman --output /boot/vmlinuz-4.19.0-batman.signed `   

	initramfs is built in the name of `vmlinuz-4.19.0-batman` not `vmlinuz-4.19.0-batman.signed`, 
	hence without reverting the name boot will fail    
	`sudo mv /boot/vmlinuz-4.19.0-batman.signed /boot/vmlinuz-4.19.0-batman`  

8. _Update grub config_

	`sudo update-grub`

9. _Reboot your system and select the signed kernel_

10. _If you want to upgrade the custom kernel, you can sign the new version easily by     
following above steps again from step seven on. Thus BACKUP the MOK-keys (MOK.der, MOK.pem, MOK.priv)._



    [Source](https://github.com/jakeday/linux-surface/blob/master/SIGNING.md)
