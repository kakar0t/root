## Flashing atmega328p-ph, atmega

#### 1. USBASP Download

USBASP 6.3, mingw32 version on windows   
Same version should work on linux also.

#### 2. USBASP connections
USBASP Pinout    
<img src="./usbasp-pinout.png" width="320">

#### 3. USBASP Setup on Atmel Studio
Tools -> External Tools

Title: `USBasp Atmega 328p`   
Command: `avrdude.exe`   
Arguments: `-c usbasp -p m328p -U flash:w:$(TargetDir)$(TargetName).hex:i`   

Atmel Studio    
<img src="./usbasp-m328p.png" width="320">

Title: `USBasp Atmega 328p Non-P`   
Command: `avrdude.exe`   
Arguments: `-c usbasp -p m328 -U flash:w:$(TargetDir)$(TargetName).hex:i`   

#### 4. Check USBASP functioning

`avrdude -c usbasp -p m328p`   

```
avrdude: warning: cannot set sck period. please check for usbasp firmware update.
avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.02s

avrdude: Device signature = 0x1e950f

avrdude: safemode: Fuses OK

avrdude done.  Thank you.
```

#### 5. Reading fuse bytes


##### m328p
`avrdude -c usbasp -p m328p -U lfuse:r:l_fuse_m328p.hex:h -U hfuse:r:h_fuse_m328p.hex:h -U efuse:r:e_fuse_m328p.hex:h`

##### m328
`avrdude -c usbasp -p m328 -U lfuse:r:l_fuse_m328.hex:h -U hfuse:r:h_fuse_m328.hex:h -U efuse:r:e_fuse_m328.hex:h`


#### 6. Writing Low-Fuse


##### m328p

`avrdude -c usbasp -p m328p -U lfuse:w:0xe2:m`   

```
Output:
avrdude: warning: cannot set sck period. please check for usbasp firmware update.
avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.01s

avrdude: Device signature = 0x1e950f
avrdude: reading input file "0xe2"
avrdude: writing lfuse (1 bytes):

Writing | ################################################## | 100% 0.01s

avrdude: 1 bytes of lfuse written
avrdude: verifying lfuse memory against 0xe2:
avrdude: load data lfuse data from input file 0xe2:
avrdude: input file 0xe2 contains 1 bytes
avrdude: reading on-chip lfuse data:

Reading | ################################################## | 100% 0.01s

avrdude: verifying ...
avrdude: 1 bytes of lfuse verified

avrdude: safemode: Fuses OK

avrdude done.  Thank you.
```

##### m328
`avrdude -c usbasp -p m328 -U lfuse:w:0xe2:m`   

```
Output:
avrdude: warning: cannot set sck period. please check for usbasp firmware update.
avrdude: AVR device initialized and ready to accept instructions

Reading | ################################################## | 100% 0.02s

avrdude: Device signature = 0x1e9514 (probably m328)
avrdude: reading input file "0xe2"
avrdude: writing lfuse (1 bytes):

Writing | ################################################## | 100% 0.01s

avrdude: 1 bytes of lfuse written
avrdude: verifying lfuse memory against 0xe2:
avrdude: load data lfuse data from input file 0xe2:
avrdude: input file 0xe2 contains 1 bytes
avrdude: reading on-chip lfuse data:

Reading | ################################################## | 100% 0.00s

avrdude: verifying ...
avrdude: 1 bytes of lfuse verified

avrdude: safemode: Fuses OK (E:FF, H:D9, L:E2)

avrdude done.  Thank you.
```
















































































































































































































