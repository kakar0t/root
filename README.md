## Look around


1. _Arduino with an ATMEGA328 Chip_    
[Arduino for production using USBASP](arduino/atmega328/usbasp)
2. _Record what's being played on ur speakers onto a MP3 file_   
[Speaker Recorder](python/speaker-recorder)
3. _Getting Started with RPi_   
[Getting Started](raspberrypi/documents/getting-started.pdf)
4. _Bare Metal UART and GPIO Drivers for RPi_   
[Bare Bones](raspberrypi/bare-bones)
5. _Lots of RPi documentation_   
[Documentation](raspberrypi/documents)
6. _RC files and CMD Cheatsheets_   
[Cheats](sidekick/cheatsheets)
7. _Scripts - RPi Kernel Builder, CScope with VIM, etc.,_   
[Scripts](sidekick/scripts)
8. _Class D amplifier_    
[Class D Amplifier](hardware/TPA3122/)   

    Unpolulated Board   
    <img src="hardware/TPA3122/TPA3122_Class-D_Top.png" width="480">   
    
    Populated Board   
    <a href="Populated Board"><img src="hardware/TPA3122/populated.jpg" align="center" height="480" width="640" ></a>

9. _A dumb Clock_    
[Clock](hardware/CLK/)   
 
    Preview   
    <img src="hardware/CLK/schematics/preview.jpg" width="480">   
    
    PCB   
    <img src="hardware/CLK/schematics/clock_top.png" width="480">   

10. _Signing a EFI Kernel_   
[Custom kernel signing](signing-custom-kernel/)